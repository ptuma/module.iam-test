// user groups
resource "aws_iam_group_membership" "group_memberships" {
  for_each   = var.groups
  depends_on = [aws_iam_user.users]
  name       = "${each.value.name}_group_membership"
  users      = [for i, z in var.users : i if contains(z.groups, each.value.name)]
  group      = each.value.name
}

// group policies
resource "aws_iam_group_policy_attachment" "group-direct-policy" {
  for_each   = var.group_policy_attachments
  depends_on = [aws_iam_user.users]
  group      = each.value.group
  policy_arn = each.value.policy_arn
}

// groups
resource "aws_iam_group" "groups" {
  for_each = var.groups
  name     = each.value.name
}

// users
resource "aws_iam_user" "users" {
  for_each      = var.users
  name          = each.key
  force_destroy = true
  tags          = var.tags
}

// access keys
resource "aws_iam_access_key" "users" {
  depends_on = [aws_iam_user.users]
  for_each   = var.users
  user       = aws_iam_user.users[each.key].name
}

data "aws_iam_policy_document" "developers_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["arn:aws:iam::691961290356:root"]
      type        = "AWS"
    }
  }

}

data "aws_iam_policy_document" "developers_assume_group_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    resources = [aws_iam_role.developers-role.arn]
  }

}

resource "aws_iam_policy" "developers" {
  name   = "Temify-developers_assume_role"
  policy = data.aws_iam_policy_document.developers_assume_group_policy.json
}

resource "aws_iam_group_policy_attachment" "developers" {
  group      = "Temify"
  policy_arn = aws_iam_policy.developers.arn
}

resource "aws_iam_role" "developers-role" {
  name               = "Temify-developers_role"
  assume_role_policy = data.aws_iam_policy_document.developers_assume_role_policy.json
}



