variable "tags" {
  type    = map(string)
  default = {}
}

variable "roles" {
  description = "Map of custom roles to be created."
  type = map(object({
    name                = string
    trusted_identifiers = set(string)
    trusted_services    = set(string)
  }))
  default = {}
}

variable "users" {
  description = "Map of custom users to be created."
  type = map(object({
    name   = string
    groups = list(string)
  }))
  default = {}
}

variable "groups" {
  description = "Map of custom groups to be created."
  type = map(object({
    name = string
  }))
  default = {}
}

variable "role_policy_attachments" {
  description = "flatten map of policy to role attachments"
  type = map(object({
    role       = string
    policy_arn = string
  }))
  default = {}
}

variable "user_policy_attachments" {
  description = "flatten map of policy to user attachments"
  type = map(object({
    user       = string
    policy_arn = string
  }))
  default = {}
}

variable "group_policy_attachments" {
  description = "flatten map of policy to group attachments"
  type = map(object({
    group      = string
    policy_arn = string
  }))
  default = {}
}
