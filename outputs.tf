output "groups" {
  description = "Groups"
  value       = aws_iam_group.groups
}

output "users" {
  description = "Users"
  value       = aws_iam_user.users
}

output "access_keys" {
  description = "Access Keys"
  value       = aws_iam_access_key.users.*
  sensitive   = false
}
